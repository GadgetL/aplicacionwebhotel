<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('api_perrop','PruebaConsumoController\ConsumoControlador@consumo_Busqueda');
Route::get('api_perro_insertarp','PruebaConsumoController\ConsumoControlador@api_Registra');
Route::get('api_perro_actualizarp','PruebaConsumoController\ConsumoControlador@api_Actualiza');
Route::get('api_perro_bajap','PruebaConsumoController\ConsumoControlador@api_Baja');
Route::get('api_perro_eliminarp','PruebaConsumoController\ConsumoControlador@api_Eliminar');
