<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('ver_Apip','PruebaApiController\ApiControlador@ver_Api');
Route::post('ver_Api_Postp','PruebaApiController\ApiControlador@ver_Api_Post');
Route::post('insertar_Apip','PruebaApiController\ApiControlador@insertar_Api');
Route::put('actualizar_Apip','PruebaApiController\ApiControlador@actualizar_Api');
Route::put('baja_Apip','PruebaApiController\ApiControlador@baja_Api');
Route::put('eliminar_Apip','PruebaApiController\ApiControlador@eliminar_Api');
