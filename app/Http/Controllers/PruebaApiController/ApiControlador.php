<?php
namespace App\Http\Controllers\PruebaApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;
//use GuzzleHttp\Client;
use App\Models\PruebaModelo\modelo_perro;

class ApiControlador extends Controller{
  public function ver_Api() // api tipo request parametro = INSEGURO =
  {
    //$raza = $_REQUEST["raza"];
    $raza = "vainillita";
    $consulta_Ver = modelo_perro::select('nombre','raza')
    ->where('raza',$raza)->get();
    return $consulta_Ver;
  }

  public function ver_Api_Post(Request $request) // api tipo form www urlenconded = SAVE =
  {
    $raza = $request->input('tipo_raza'); // uds especifican la variable virtual
    $consulta_Ver = modelo_perro::select('nombre','raza')
    ->where('raza',$raza)->get();
    return $consulta_Ver;
  }

  public function insertar_Api(Request $variable)
  {
    $nombre = $variable->nombre; // o $variable->input('nombre');
    $raza = $variable->raza; // o $variable->input('raza');
    modelo_perro::create(['nombre' => $nombre, 'raza' => $raza]);
    return response()->json(['respuesta' => 'Registrado correctamente']);
  }

  public function actualizar_Api(Request $variable)
  {
      $nombre = $variable->nombre; // o $variable->input('nombre');
      $raza = $variable->raza; // o $variable->input('raza');
      modelo_perro::where('raza','=',$raza)->update(['nombre' => $nombre, 'raza' => $raza]);
      return response()->json(['mensaje' => 'Actualizado correctamente']);
  }

  public function baja_Api(Request $variable)
  {
      $estatus = "INACTIVO"; // o $variable->input('estatus');
      $raza = $variable->raza; // o $variable->input('raza');
      modelo_perro::where('raza','=',$raza)->update(['estatus' => $estatus]);
      return response()->json(['mensaje' => 'Dado de baja correctamente']);
  }

  public function eliminar_Api(Request $variable)
  {
      $raza = $variable->raza; // o $variable->input('raza');
      modelo_perro::where('raza','=',$raza)->delete();
      return response()->json(['mensaje' => 'Eliminado correctamente']);
  }
}
?>
