<?php

namespace App\Http\Controllers\PruebaConsumoController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Iluminate\Support\Facades\Input;
use GuzzleHttp\Client;
use App\Models\PruebaModelo\modelo_perro;

class ConsumoControlador extends Controller{
  public function consumo_Busqueda()
  {
    $respuesta = $this->peticion('GET',"http://hotel.ito:81/api/ver_Apip",[
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'X-Requested-With' => 'XMLHttpRequest'
      ],
      'form_params' => [
        'raza' => 'vainillita'
      ]
    ]);
    $retorno_datos = json_decode($respuesta);
    return response()->json($retorno_datos);
  }

  public function api_Registra()
  {
      $respuesta = $this->peticion('POST',"http://hotel.ito:81/api/insertar_Apip",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'nombre' => 'Becerrito',
          'raza' => 'Chihuahua'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Actualiza()
  {
      $respuesta = $this->peticion('PUT',"http://hotel.ito:81/api/actualizar_Apip",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'nombre' => 'PATITO',
          'raza' => 'pitbull'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Baja()
  {
      $respuesta = $this->peticion('PUT',"http://hotel.ito:81/api/baja_Apip",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'raza' => 'Poodle'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }

  public function api_Eliminar()
  {
      $respuesta = $this->peticion('PUT',"http://hotel.ito:81/api/eliminar_Apip",[
        'headers' => [
           'Content-Type' => 'application/x-www-form-urlencoded',
           'X-Requested-With' => 'XMLHttpRequest'
        ],
        'form_params' => [
          'raza' => 'Poodle'
        ]
      ]);
      $datos = json_decode($respuesta);

      return response()->json($datos);
  }
}
?>
